# Личный кабинет панель менеджера

```plantuml
@startuml Личный кабинет панель менеджера

!include <C4/C4>
!include <C4/C4_Context>
!include <C4/C4_Container>

!include https://gitlab.com/soprun/arch/-/raw/main/services/04_domen_client_servis/LK-manager-panel/LK-manager-panel.puml
!include https://gitlab.com/soprun/arch/-/raw/main/services/04_domen_client_servis/LK-manager-panel/LK-manager-panel-container.puml

@endum
```