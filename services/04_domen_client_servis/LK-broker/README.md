# Личный кабинет брокера

```plantuml
@startuml Личный кабинет брокера

!include <C4/C4>
!include <C4/C4_Context>
!include <C4/C4_Container>

!include https://gitlab.com/soprun/arch/-/raw/main/services/04_domen_client_servis/LK-broker/LK-broker.puml
!include https://gitlab.com/soprun/arch/-/raw/main/services/04_domen_client_servis/LK-broker/LK-broker-container.puml

@endum
```