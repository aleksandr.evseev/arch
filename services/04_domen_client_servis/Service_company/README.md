# MIS service

[[_TOC_]]

# Header 1

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed commodo tincidunt tellus, non auctor ex finibus ut. Sed sit
amet condimentum nisi, non iaculis nibh. Maecenas accumsan suscipit pulvinar. Duis porttitor lacinia enim, quis maximus
tortor aliquet et. Mauris tempor finibus sapien, ut cursus neque vulputate nec. Nam laoreet augue a leo semper, at
faucibus quam pulvinar. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aenean
fringilla tellus vitae lobortis sollicitudin. In pellentesque turpis eleifend magna consequat tincidunt. Maecenas turpis
orci, accumsan ut vehicula vitae, sagittis eget nunc. In eleifend ultricies dictum. Nullam ornare imperdiet commodo.

## Software MIS - System Container

```plantuml
@startuml MIS architecture design

!include <C4/C4>
!include <C4/C4_Context>
!include <C4/C4_Container>

!include https://gitlab.com/soprun/arch/-/raw/main/services/04_domen_client_servis/Service_company/MIS.puml
@endum
```

## Software MIS - System Component

```plantuml
@startuml MIS architecture design

!include <C4/C4>
!include <C4/C4_Context>
!include <C4/C4_Container>
!include <C4/C4_Component>

!include https://gitlab.com/soprun/arch/-/raw/main/services/04_domen_client_servis/Service_company/MIS.puml
!include https://gitlab.com/soprun/arch/-/raw/main/services/04_domen_client_servis/Service_company/MIS-сomponent.puml
@endum
```


## Software MIS - ER-диаграмма
```plantuml
@startuml - ER-диаграмма
!include <C4/C4>
!include <C4/C4_Context>
!include <C4/C4_Container>
!include https://gitlab.com/soprun/arch/-/raw/main/services/04_domen_client_servis/Service_company/MIS-ER.puml
!include https://gitlab.com/soprun/arch/-/raw/main/services/04_domen_client_servis/Service_company/MIS-ER-2.puml
@endum
```
## Software MIS - Диаграмма последовательности
```plantuml
@startuml - SD
!include <C4/C4>
!include <C4/C4_Context>
!include <C4/C4_Container>
!include https://gitlab.com/soprun/arch/-/raw/main/services/04_domen_client_servis/Service_company/MIS-SD.puml
@endum
```

## Software MIS - use cases
```plantuml
@startuml - US
!include <C4/C4>
!include <C4/C4_Context>
!include <C4/C4_Container>
!include https://gitlab.com/soprun/arch/-/raw/main/services/04_domen_client_servis/Service_company/MIS-UC.puml
@endum
```