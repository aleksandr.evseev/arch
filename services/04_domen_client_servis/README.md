# Каталог сервисов клиентского сервиса

[[_TOC_]]

---

# Getting started

- Ответственный за направление: [Сопрун Владислав](@soprun)

## Портфель проектов

- [ ] [Личный кабинет брокера](services/04_domen_client_servis/LK-broker)
- [ ] [Личный кабинет клиента](services/04_domen_client_servis/LK-client)
- [ ] [Личный кабинет панель менеджера](services/04_domen_client_servis/LK-manager-panel)
- [ ] [Портал (сайт strana.com)](services/04_domen_client_servis/portal)
- [ ] [Динамическое ценообразование (ДЦО)](services/04_domen_client_servis/DCO)


```plantuml
set separator none
title Архитектура сервисов клиентского сервиса

!include <C4/C4>
!include <C4/C4_Context>
!include <C4/C4_Container>

!include https://gitlab.com/soprun/arch/-/raw/main/services/04_domen_client_servis/context.puml

!include https://gitlab.com/soprun/arch/-/raw/main/services/04_domen_client_servis/LK-broker/LK-broker-container.puml
!include https://gitlab.com/soprun/arch/-/raw/main/services/04_domen_client_servis/LK-client/LK-client-container.puml
!include https://gitlab.com/soprun/arch/-/raw/main/services/04_domen_client_servis/LK-manager-panel/LK-manager-panel-container.puml
!include https://gitlab.com/soprun/arch/-/raw/main/services/04_domen_client_servis/portal/portal-container.puml


```