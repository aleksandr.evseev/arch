# arch

## Getting started

### Software System - System Context

```plantuml
set separator none
title Software System - System Context

top to bottom direction

!include <C4/C4>
!include <C4/C4_Context>

Person(User, "User", "A user of my software system.", $tags="")
Person(Employee, "Employee", "", $tags="")
System(Sentry, "Sentry", "Sentry system.", $tags="")
System(Datareon1СПредприятие, "Datareon 1С:Предприятие", "Enterprise Service Bus system.", $tags="")
System(amoCRM, "amoCRM", "??? system.", $tags="")
System(SoftwareSystem, "Software System", "My software system.", $tags="")

Rel_D(amoCRM, SoftwareSystem, "Get leads", $tags="")
Rel_D(User, SoftwareSystem, "Uses web application MIS", $tags="")
Rel_D(Employee, SoftwareSystem, "Uses web application MIS", $tags="")
Rel_D(Datareon1СПредприятие, SoftwareSystem, "обмен сообщениями", $tags="")
Rel_D(SoftwareSystem, Sentry, "Client error", $tags="")

SHOW_LEGEND(true)
```

### Software System - Containers


```plantuml
set separator none
title Software System - Containers

top to bottom direction

!include <C4/C4>
!include <C4/C4_Context>
!include <C4/C4_Container>

Person(User, "User", "A user of my software system.", $tags="")
Person(Employee, "Employee", "", $tags="")
System(Sentry, "Sentry", "Sentry system.", $tags="")
System(Datareon1СПредприятие, "Datareon 1С:Предприятие", "Enterprise Service Bus system.", $tags="")
System(amoCRM, "amoCRM", "??? system.", $tags="")

System_Boundary("SoftwareSystem_boundary", "Software System", $tags="") {
  Boundary(group_1, "CLI", $tags="CLI") {
    Container(SoftwareSystem.CLIimportamoCRM, "CLI import amoCRM", "python, cron", $tags="")
    Container(SoftwareSystem.CLIprepare, "CLI prepare", "python, cron, producer", $tags="")
    Container(SoftwareSystem.CLIscoring, "CLI scoring", "python, cron, comsumer", $tags="")
    Container(SoftwareSystem.CLIprocessing, "CLI processing", "python, cron", $tags="")
  }

  ContainerDb(SoftwareSystem.Database, "Database", "PostgreSQL", $tags="")
  ContainerQueue(SoftwareSystem.EventBus, "Event Bus", "Apache Kafka", $tags="")
  ContainerDb(SoftwareSystem.BI, "BI", "ClickHouse", $tags="")
  Container(SoftwareSystem.APIbackend, "API backend", "Python, FastAPI", $tags="")
  Container(SoftwareSystem.WebApplicationMIS, "Web Application MIS", "MIS", "Информационная система MIS", $tags="")
}

Rel_D(SoftwareSystem.WebApplicationMIS, SoftwareSystem.APIbackend, "Uses API", "HTTPs, REST", $tags="")
Rel_D(SoftwareSystem.APIbackend, SoftwareSystem.Database, "Reads from and writes to", $tags="")
Rel_D(SoftwareSystem.APIbackend, SoftwareSystem.BI, "Reads from and writes to", $tags="")
Rel_D(SoftwareSystem.APIbackend, Sentry, "Send API error", $tags="")
Rel_D(SoftwareSystem.Database, SoftwareSystem.APIbackend, "", $tags="")
Rel_D(amoCRM, SoftwareSystem.CLIimportamoCRM, "Get leads", $tags="")
Rel_D(SoftwareSystem.CLIimportamoCRM, SoftwareSystem.Database, "save leads", $tags="")
Rel_D(SoftwareSystem.CLIimportamoCRM, Sentry, "Send CLI error", $tags="")
Rel_D(SoftwareSystem.Database, SoftwareSystem.CLIprepare, "Reads from and writes to", $tags="")
Rel_D(SoftwareSystem.CLIprepare, SoftwareSystem.EventBus, "Reads from and writes to", $tags="")
Rel_D(SoftwareSystem.CLIprepare, Sentry, "Send CLI error", $tags="")
Rel_D(SoftwareSystem.Database, SoftwareSystem.CLIscoring, "Reads from and writes to", $tags="")
Rel_D(SoftwareSystem.EventBus, SoftwareSystem.CLIscoring, "Reads from and writes to", $tags="")
Rel_D(SoftwareSystem.CLIscoring, SoftwareSystem.BI, "Reads from and writes to", $tags="")
Rel_D(SoftwareSystem.CLIscoring, Sentry, "Send CLI error", $tags="")
Rel_D(SoftwareSystem.Database, SoftwareSystem.CLIprocessing, "Reads from and writes to", $tags="")
Rel_D(SoftwareSystem.CLIprocessing, SoftwareSystem.Database, "Reads from and writes to", $tags="")
Rel_D(SoftwareSystem.CLIprocessing, Sentry, "Send CLI error", $tags="")
Rel_D(User, SoftwareSystem.WebApplicationMIS, "Uses web application MIS", $tags="")
Rel_D(Employee, SoftwareSystem.WebApplicationMIS, "Uses web application MIS", $tags="")
Rel_D(Datareon1СПредприятие, SoftwareSystem.Database, "обмен сообщениями", $tags="")
Rel_D(SoftwareSystem.WebApplicationMIS, Sentry, "Client error", $tags="")

SHOW_LEGEND(true)
```

### Software System - API backend - Components

```plantuml
set separator none
title Software System - API backend - Components

top to bottom direction

!include <C4/C4>
!include <C4/C4_Context>
!include <C4/C4_Container>
!include <C4/C4_Component>

ContainerDb(SoftwareSystem.Database, "Database", "PostgreSQL", $tags="")

Container_Boundary("SoftwareSystem.APIbackend_boundary", "API backend", $tags="") {
  Component(SoftwareSystem.APIbackend.Системамониторинганаполнениясчетовэскроу, "Система мониторинга наполнения счетов эскроу", "", $tags="")
  Component(SoftwareSystem.APIbackend.Dashboards, "Dashboards", "", $tags="")
  Component(SoftwareSystem.APIbackend.Projects, "Projects", "", $tags="")
  Component(SoftwareSystem.APIbackend.Bestemployees, "Best employees", "", $tags="")
  Component(SoftwareSystem.APIbackend.PowerBI, "Power BI", "", $tags="")
  Component(SoftwareSystem.APIbackend.News, "News", "", $tags="")
  Component(SoftwareSystem.APIbackend.Articles, "Articles", "", $tags="")
  Component(SoftwareSystem.APIbackend.Ideas, "Ideas", "", $tags="")
  Component(SoftwareSystem.APIbackend.ORGstructure, "ORG structure", "", $tags="")
}

Rel_D(SoftwareSystem.Database, SoftwareSystem.APIbackend.Системамониторинганаполнениясчетовэскроу, "", $tags="")

SHOW_LEGEND(true)
```